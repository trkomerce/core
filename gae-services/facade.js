const firebase = require('./firebase');

class FirebaseFacade {
  async createUser(email, password) {
    const userRecord = await firebase.auth.createUser({
      email,
      password,
    });
    return userRecord.toJSON();
  }

  async createDocument(collectionName, data) {
    const docRef = await firebase.firestore.collection(collectionName).add(data);
    const doc = await docRef.get();
    return {
      id: doc.id,
      ...doc.data(),
    };
  }

  async getDocument(collectionName, documentId) {
    const docRef = await firebase.firestore.collection(collectionName).doc(documentId).get();
    if (docRef.exists) {
      return {
        id: docRef.id,
        ...docRef.data(),
      };
    }
    return null;
  }

  async updateDocument(collectionName, documentId, data) {
    const docRef = await firebase.firestore.collection(collectionName).doc(documentId);
    await docRef.update(data);
    const doc = await docRef.get();
    return {
      id: doc.id,
      ...doc.data(),
    };
  }

  async deleteDocument(collectionName, documentId) {
    await firebase.firestore.collection(collectionName).doc(documentId).delete();
 
