const firebaseAdmin = require('firebase-admin');
const serviceAccount = require('./service-account.json');

firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(serviceAccount),
  databaseURL: `https://${process.env.FIREBASE_PROJECT_ID}.firebaseio.com`,
});

module.exports = {
  auth: firebaseAdmin.auth(),
  firestore: firebaseAdmin.firestore(),
};
